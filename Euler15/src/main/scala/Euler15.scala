import java.awt.Point

import MyDirection.MyDirection
import ch.qos.logback.classic.Level
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.math.BigInt


object Euler15 extends App {
  private val loggerInstance = LoggerFactory.getLogger(Euler15.getClass.getSimpleName)
    .asInstanceOf[ch.qos.logback.classic.Logger]

  loggerInstance.setLevel(Level.DEBUG)
  val logger = Logger(loggerInstance)

  logger.info("Starting Lattice Pather...")

  for (i <- 1 to 20) {
    val permutationsCount = binomialCoefficient(BigInt(i * 2), BigInt(i)) * binomialCoefficient(BigInt(i), BigInt(i))

    logger.info(s"There are ${permutationsCount} paths for a ${i}x$i square.")
  }

  def getNumberOfPaths(numberOfSquares: Int): Int = {
    var canPath = true
    var paths = Array[Array[MyDirection]]()

    // If I have an array of booleans with an even length
    // half are true and half are false
    // How many unique sequences of booleans are there?


    //    while (canPath) {
    //      val matrixOfVerticies = initMatrix(numberOfSquares)
    //
    //      val position = new Point(0, 0)
    //      var path = Array[MyDirection]()
    //
    //      var pathIndex = 0
    //      while (position.x < numberOfSquares || position.y < numberOfSquares) {
    //        if (position.x < numberOfSquares) {
    //          path :+= move(position, MyDirection.Right, matrixOfVerticies)
    //        } else if (position.y < numberOfSquares) {
    //          path :+= move(position, MyDirection.Down, matrixOfVerticies)
    //        }
    //        pathIndex += 1
    //      }
    //      paths :+= path
    //
    //      var pathStr = "Path [ "
    //      for (pathMarker <- path) {
    //        pathStr += pathMarker + " "
    //      }
    //      pathStr += "Done! ] " + path.length
    //
    //      println(pathStr)
    //      printMatrix(matrixOfVerticies)
    //      // TODO
    //      canPath = paths.length < numberOfSquares
    //    }

    paths.length
  }

  def binomialCoefficient(top: BigInt, bottom: BigInt): BigInt = {
    // https://math.stackexchange.com/questions/175621/distinct-permutations-of-the-word-toffee
    val t = factorial(top)
    val b = factorial(bottom) * factorial(top - bottom)
    t / b
  }

  @annotation.tailrec
  def recursive_factorial(number: BigInt, result: BigInt = 1): BigInt = {
    if (number <= BigInt(1))
      result
    else
      recursive_factorial(number - 1, result * number)
  }

  def factorial(n: BigInt): BigInt = {
    if (n <= BigInt(0))
      1
    else
      n * factorial(n - 1)
  }

  def permutations(numberOfPathSteps: Int): Int = {
    var sequences = List.fill(numberOfPathSteps * 2)(0 to 1)

    val sequences2 = sequences.flatten.combinations(numberOfPathSteps * 2).flatMap(_.permutations)

    val uniqueSequences = sequences2.filter(_.sum == numberOfPathSteps)

    uniqueSequences.length
  }

  def permutations2(numberOfPathSteps: Int): Int = {
    val intses: Iterator[List[Int]] = getI(numberOfPathSteps)

    intses.flatMap(_.permutations).size
  }

  private def getI(numberOfPathSteps: Int) = {
    List.fill(numberOfPathSteps * 2)(0 to 1).flatten
      .combinations(numberOfPathSteps * 2)
      .filter(_.sum == numberOfPathSteps)
  }

  def permute(numberOfPathSteps: Int): Int = {
    val permutation: Array[Int] = getI(numberOfPathSteps: Int).take(1).toList.head.toArray

    var length = permutation.length
    var result = mutable.HashSet[String]()
    result.add(permutation.clone().mkString(","))
    var c = List.fill(length)(0).toArray
    var i = 1
    var k = 0

    while (i < length) {
      if (c(i) < i) {
        k = 0
        if (i % 2 > 0) {
          k = c(i)
        }

        var p = permutation(i)
        permutation(i) = permutation(k)
        permutation(k) = p
        c(i) += 1
        i = 1
        result.add(permutation.clone().mkString(","))
      } else {
        c(i) = 0
        i += 1
      }
    }

    result.size
  }

  // https://stackoverflow.com/questions/9960908/permutations-in-javascript/37580979#37580979

  def move(position: Point, direction: MyDirection, matrixYX: Array[Array[MyDirection]]) = {
    if (direction == MyDirection.Right) {
      matrixYX(position.y)(position.x) = direction
      position.x += 1
    } else if (direction == MyDirection.Down) {
      matrixYX(position.y)(position.x) = direction
      position.y += 1
    }
    direction
  }

  def initMatrix(numberOfSquares: Int) = {
    val matrix = Array.ofDim[MyDirection](numberOfSquares + 1, numberOfSquares + 1)
    for (matrixRow <- matrix) {
      for (i <- matrixRow.indices) {
        matrixRow(i) = MyDirection.Unused
      }
    }
    matrix
  }

  def printMatrix(matrixYX: Array[Array[MyDirection]]): Unit = {
    if (loggerInstance.isDebugEnabled()) {
      var str = ""
      for (matrixRow <- matrixYX) {
        for (matrixColumn <- matrixRow) {
          str += matrixColumn + " "
        }
        str += "\n"
      }
      println(str)
    }
  }


}

object MyDirection extends Enumeration {
  type MyDirection = Value
  val Unused = Value("+")
  val Right = Value(">")
  val Down = Value("v")
  val Done = Value("O")
}
