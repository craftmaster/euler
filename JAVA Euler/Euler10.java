import java.math.BigInteger;


public class Euler10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BigInteger primeSum = new BigInteger("2");
	
		long currentPrime = 1;
		long currentTest = Long.valueOf(currentPrime);
		while(currentTest <= 2_000_000){
			if(isPrime(currentTest)){
				currentPrime = Long.valueOf(currentTest);
				primeSum = primeSum.add(new BigInteger(currentPrime + ""));
			}
			currentTest += 2;
		}
		
		System.out.println(currentPrime);
		System.out.println(primeSum.toString());

	}
	
	public static boolean isPrime(long n) {
		if (n < 2)
			return false;
		if (n == 2 || n == 3)
			return true;
		if (n % 2 == 0 || n % 3 == 0)
			return false;
		long sqrtN = (long) Math.sqrt(n) + 1;
		for (long i = 6L; i <= sqrtN; i += 6) {
			if (n % (i - 1) == 0 || n % (i + 1) == 0)
				return false;
		}
		return true;
	}

}
