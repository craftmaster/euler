import java.util.ArrayList;

public class Euler3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Number n = new Number(9);
		System.out.println(n.largestPrimeFactor());

	}

}

class Number {
	private long thisNumber = 0;

	public Number(long i) {
		thisNumber = i;
	}

	public ArrayList<Long> factors() {
		return factors(false);
	}

	public ArrayList<Long> factors(boolean onlyPrimes) {
		long largestPrime = largestPrimeFactor();
		ArrayList<Long> factors = new ArrayList<Long>();
		for (long i = 2; i < (onlyPrimes ? largestPrime : thisNumber); i++) {
			if (thisNumber % i == 0) {
				if (!onlyPrimes) {
					factors.add(i);
				} else if (isPrime(i)) {
					factors.add(i);
				}
			}
		}
		return factors;
	}

	public boolean isPrime() {
		return isPrime(thisNumber);
	}

	public static boolean isPrime(long n) {
		if (n < 2)
			return false;
		if (n == 2 || n == 3)
			return true;
		if (n % 2 == 0 || n % 3 == 0)
			return false;
		long sqrtN = (long) Math.sqrt(n) + 1;
		for (long i = 6L; i <= sqrtN; i += 6) {
			if (n % (i - 1) == 0 || n % (i + 1) == 0)
				return false;
		}
		return true;
	}
	
	public long largestPrimeFactor(){
		return largestPrimeFactor(thisNumber);
	}
	
	public static long largestPrimeFactor(long number) {
        long i;

        for (i = 2; i <= number; i++) {
            if (number % i == 0) {
                number /= i;
                i--;
            }
        }

        return i;
    }
}