import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

public class Euler556 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		// f(10^1) = 7
		// f(10^2) = 54
		// f(10^3) = 523
		// f(10^4) = 5218
		// f(10^5) = 52127
		// f(10^6) = 521301
		// f(10^7) =
		// f(10^8) = ? 52126906
		// f(10^9) =
		// f(10^10) =
		// f(10^11) =
		// f(10^12) =
		// f(10^13) =
		// f(10^14) = ??
		outputSolution(BigInteger.valueOf(10).pow(8));
		long endTime = System.nanoTime();
		System.out
				.println((endTime - startTime) / 1_000_000_000.0 + " seconds");
	}

	/**
	 * Let f(n) be the count of proper squarefree Gaussian integers with a2 + b2
	 * ≤ n.
	 * 
	 * @param max
	 * @return
	 */
	private static BigInteger numberOfProperSquareFreeGaussianIntegersForAandB(
			BigInteger max) {
		// Setup
		BigInteger count = BigInteger.ZERO;
		int progressNumber = 10000;
		// n(n+1)/2

		JFrame f = new JFrame();
		JProgressBar j = new JProgressBar(0, progressNumber);

		j.setString("Running...");
		j.setStringPainted(true);
		f.setSize(300, 100);
		f.getContentPane().add(j, BorderLayout.NORTH);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		int progress = 1;
		BigInteger bigProgressNumber = BigInteger.valueOf(progressNumber);

		ArrayList<GaussianInteger> squarefulNumbers = new ArrayList<>();
		int squarefulNumberMax = 50;
		// Loop A
		for (BigInteger a = BigInteger.ONE; a.compareTo(max) <= 0; a = a
				.add(BigInteger.ONE)) {
			// Loop B
			BigInteger b = BigInteger.ZERO;
			GaussianInteger testNumber = new GaussianInteger(a, b);
			for (; a.compareTo(b) >= 0
					&& (testNumber.norm().compareTo(max) <= 0); b = b
					.add(BigInteger.ONE)) {

				boolean skip = false;
				for (GaussianInteger gaussianInteger : squarefulNumbers) {
					GaussianInteger quotientOfTest = testNumber
							.canEvenlyDivideBy(gaussianInteger);
					if (quotientOfTest != null) {
//						System.err.println("Squareful likely! " + testNumber
//								+ " / " + gaussianInteger + " = "
//								+ quotientOfTest);
						skip = true;
						break;
					}
				}
				if (skip) {
					if (squarefulNumbers.size() < squarefulNumberMax) {
						squarefulNumbers.add(testNumber);
					}else{
						System.err.println("Not adding number! " + testNumber);
					}
				}else{
					ArrayList<GaussianInteger> factorization = testNumber
							.getProperGaussianPrimeFactorizationSquareCheck(true);
					boolean isNumberSquarefree = factorization != null;

					if (isNumberSquarefree) {
						// System.out.println(testNumber + " is squarefree " +
						// factorization);
						count = count.add(BigInteger.ONE);

						if (!testNumber.getImaginaryInteger().equals(
								BigInteger.ZERO)
								&& !testNumber.getRealInteger().equals(
										testNumber.getImaginaryInteger())) {
							// Reverse of number will share squarity
							count = count.add(BigInteger.ONE);
						}
					} else {
//						System.err.println("Squareful number! " + testNumber);
						if (squarefulNumbers.size() < squarefulNumberMax) {
							squarefulNumbers.add(testNumber);
						}else{
							System.err.println("Not adding number! " + testNumber);
						}
					}
				}

				testNumber = new GaussianInteger(a, b.add(BigInteger.ONE));
			}
			progress = a.multiply(bigProgressNumber).divide(max).intValue();
			j.setValue(progress);
			j.setString(a + " / " + max);
		}
		f.dispose();
		return count;
	}

	private static void outputSolution(BigInteger test) {
		System.out.println("f(" + test + ") = "
				+ numberOfProperSquareFreeGaussianIntegersForAandB(test));
	}
}

class GaussianInteger {
	public static final GaussianInteger GAUSSIAN_UNIT_1 = new GaussianInteger(
			"1", "0");
	public static final GaussianInteger GAUSSIAN_UNIT_I = new GaussianInteger(
			"0", "1");

	public static final GaussianInteger GAUSSIAN_UNIT_NEG_1 = new GaussianInteger(
			"-1", "0");

	public static final GaussianInteger GAUSSIAN_UNIT_NEG_I = new GaussianInteger(
			"0", "-1");

	public static final GaussianInteger[] GAUSSIAN_UNITS = new GaussianInteger[] {
			GAUSSIAN_UNIT_1, GAUSSIAN_UNIT_I, GAUSSIAN_UNIT_NEG_1,
			GAUSSIAN_UNIT_NEG_I };

	/**
	 * Get prime factors of a regular integer
	 * 
	 * @param initialNumber
	 * @return
	 */
	private static ArrayList<GaussianInteger> getGaussianPrimeFactorizationOfRealNumber(
			BigInteger initialNumber) {
		if (initialNumber.equals(IntegerUtils.ZERO)) {
			return new ArrayList<>();
		}

		GaussianInteger possibleUnit = new GaussianInteger(initialNumber);
		if (possibleUnit.isGaussianUnit()) {
			return new ArrayList<>(
					Arrays.asList(new GaussianInteger[] { possibleUnit }));
		}

		ArrayList<BigInteger> integerPrimeFactors = IntegerUtils
				.primeFactors(initialNumber);
		// System.out.println("Prime factors of " + initialNumber + " are " +
		// integerPrimeFactors);
		// if (!initialNumber.equals(Integers.ONE)) {
		// integerPrimeFactors.add(initialNumber);
		// System.out.println(initialNumber);
		// }
		ArrayList<GaussianInteger> gaussianPrimeFactors = new ArrayList<>();
		for (BigInteger bigInteger : integerPrimeFactors) {
			if (bigInteger.equals(IntegerUtils.TWO)) {
				gaussianPrimeFactors.add(new GaussianInteger(1, 1));
				gaussianPrimeFactors.add(new GaussianInteger(1, -1));
			} else if (bigInteger.mod(IntegerUtils.FOUR).equals(
					IntegerUtils.THREE)) {
				gaussianPrimeFactors.add(new GaussianInteger(bigInteger));
			} else {
				BigInteger n = IntegerUtils.ONE;
				BigInteger sqrt;
				while (true) {
					BigInteger test = bigInteger.subtract(n.pow(2));
					sqrt = IntegerUtils.bigIntSqRootFloor(test);
					if (sqrt.pow(2).equals(test)) {
						break;
					}
					n = n.add(IntegerUtils.ONE);
				}
				gaussianPrimeFactors.add(new GaussianInteger(n, sqrt));
				gaussianPrimeFactors.add(new GaussianInteger(n, sqrt.negate()));
			}
			// System.out.println("n = " + bigInteger);
		}

		return gaussianPrimeFactors;
	}

	private static BigInteger multiplyByConjugateForNormalizedInteger(
			GaussianInteger reducedGaussianNumber) {
		return reducedGaussianNumber
				.multiply(reducedGaussianNumber.conjugate()).getRealInteger();
	}

	/**
	 * Multiply all the factors together and return the product. Can be used to
	 * test prime factorization.
	 * 
	 * @param factors
	 * @return
	 */
	public static GaussianInteger product(ArrayList<GaussianInteger> factors) {
		if (factors.size() == 0) {
			return new GaussianInteger(0);
		}
		GaussianInteger current = new GaussianInteger(1);
		for (GaussianInteger bigComplex : factors) {
			current = current.multiply(bigComplex);
		}
		return current;
	}

	private final BigInteger imaginaryInteger; // the imaginary part

	private final BigInteger realInteger; // the real part

	public GaussianInteger(BigInteger real) {
		realInteger = real;
		imaginaryInteger = BigInteger.ZERO;
	}

	public GaussianInteger(BigInteger real, BigInteger imaginary) {
		realInteger = real;
		imaginaryInteger = imaginary;
	}

	public GaussianInteger(long real) {
		realInteger = BigInteger.valueOf(real);
		imaginaryInteger = BigInteger.ZERO;
	}

	public GaussianInteger(long real, long imaginary) {
		realInteger = BigInteger.valueOf(real);
		imaginaryInteger = BigInteger.valueOf(imaginary);
	}

	public GaussianInteger(String real) {
		realInteger = new BigInteger(real);
		imaginaryInteger = BigInteger.ZERO;
	}

	public GaussianInteger(String real, String imaginary) {
		realInteger = new BigInteger(real);
		imaginaryInteger = new BigInteger(imaginary);
	}

	/**
	 * GuassianDividend has a no remainder division with GuassianDivisor if
	 * (dividendReal * divisorReal + dividendImaginary * divisorImaginary) has a
	 * no remainder division with (divisorReal^2 + divisorImaginary^2) and
	 * (dividendReal * divisorReal - dividendImaginary * divisorImaginary) has a
	 * no remainder division with (divisorReal^2 + divisorImaginary^2) NOTE: the
	 * minor difference of subtraction
	 * 
	 * @param dividend
	 * @param divisor
	 * @return null if not evenly divisible or the answer to the division
	 */
	public GaussianInteger canEvenlyDivideBy(GaussianInteger divisor) {
		GaussianInteger dividend = this;
		BigInteger a1xa2 = dividend.getRealInteger().multiply(
				divisor.getRealInteger());
		BigInteger b1xb2 = dividend.getImaginaryInteger().multiply(
				divisor.getImaginaryInteger());
		BigInteger a2S_plus_b2S = divisor.getRealInteger().pow(2)
				.add(divisor.getImaginaryInteger().pow(2));
		if (a2S_plus_b2S.equals(BigInteger.ZERO)) {
			return null;
		}
		BigInteger[] tempArray = a1xa2.add(b1xb2).divideAndRemainder(
				a2S_plus_b2S);

		BigInteger realNumber = tempArray[0];
		BigInteger realRemainder = tempArray[1];
		if (!realRemainder.equals(BigInteger.ZERO)) {
			return null;
		}

		BigInteger a2xb1 = divisor.getRealInteger().multiply(
				dividend.getImaginaryInteger());

		BigInteger a1xb2 = dividend.getRealInteger().multiply(
				divisor.getImaginaryInteger());

		tempArray = a2xb1.subtract(a1xb2).divideAndRemainder(a2S_plus_b2S);
		BigInteger imaginaryNumber = tempArray[0];
		BigInteger imaginaryRemainder = tempArray[1];
		if (!imaginaryRemainder.equals(BigInteger.ZERO)) {
			return null;
		}

		return new GaussianInteger(realNumber, imaginaryNumber);
	}

	// return a new Complex object whose value is the conjugate of this
	public GaussianInteger conjugate() {
		return new GaussianInteger(getRealInteger(), getImaginaryInteger()
				.negate());
	}

	/**
	 * For a Gaussian Number z = a + bi, return b + ai;
	 * 
	 * @return
	 */
	public GaussianInteger reverse() {
		return new GaussianInteger(getImaginaryInteger(), getRealInteger());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BigInteger) {
			if (this.isRealNumber()) {
				return (this.getRealInteger().equals(obj));
			}
		}
		if (obj instanceof GaussianInteger) {
			GaussianInteger b = ((GaussianInteger) obj);
			return this.getRealInteger().equals(b.getRealInteger())
					&& this.getImaginaryInteger().equals(
							b.getImaginaryInteger());
		}
		return false;
	}

	/**
	 * 
	 * @param squarefulCheck
	 *            Return null if number is squareful or zero
	 * @return an arraylist of factors List will be empty if number is zero or a
	 *         unit
	 */
	public ArrayList<GaussianInteger> getProperGaussianPrimeFactorizationSquareCheck(
			boolean squarefulCheck) {

		ArrayList<GaussianInteger> possibleGaussianPrimeFactors = new ArrayList<>();
		ArrayList<GaussianInteger> possibleUnits = new ArrayList<>();

		// Do some setup
		BigInteger normalInteger;
		if (this.isZero()) {
			if (squarefulCheck) {
				return null;
			}
			return new ArrayList<>();
		} else if (this.isGaussianUnit()) {
			return new ArrayList<>();
		} else if (this.isRealNumber()) {
			normalInteger = getRealInteger();
		} else if (this.getRealInteger().equals(BigInteger.ZERO)) {
			normalInteger = this.getImaginaryInteger();
			possibleUnits.add(GAUSSIAN_UNIT_I);
		} else {
			normalInteger = multiplyByConjugateForNormalizedInteger(this);
		}
		if (squarefulCheck && this.isPrime()) {
			return new ArrayList<>();
		}

		// Get Integer Prime Factors
		ArrayList<BigInteger> primeFactors = IntegerUtils
				.primeFactors(normalInteger);

		// Get Possible Gaussian Prime Factors

		for (BigInteger factor : primeFactors) {
			if (factor.mod(IntegerUtils.FOUR).equals(IntegerUtils.THREE)) {
				GaussianInteger e = new GaussianInteger(factor);
				possibleGaussianPrimeFactors.add(e);
			} else {
				BigInteger n = IntegerUtils.ONE;
				BigInteger sqrt;
				while (true) {
					BigInteger test = factor.subtract(n.pow(2));
					sqrt = IntegerUtils.bigIntSqRootFloor(test);
					if (sqrt.pow(2).equals(test)) {
						break;
					}
					n = n.add(IntegerUtils.ONE);
				}
				GaussianInteger e1 = new GaussianInteger(n, sqrt);
				possibleGaussianPrimeFactors.add(e1);
				if (!sqrt.equals(BigInteger.ZERO)) {
					GaussianInteger e2 = new GaussianInteger(n, sqrt.negate());
					possibleGaussianPrimeFactors.add(e2);
				}
			}
		}

		// Test possible factors against gaussian number
		ArrayList<GaussianInteger> gaussianFactors = new ArrayList<>();
		GaussianInteger n = this;
		for (GaussianInteger factor : possibleGaussianPrimeFactors) {
			GaussianInteger x = n.canEvenlyDivideBy(factor);
			if (x != null) {
				n = x;
				// Divide out units
				for (GaussianInteger unit : GAUSSIAN_UNITS) {
					GaussianInteger r = factor.canEvenlyDivideBy(unit);
					if (r != null && r.isProperGuassian()) {
						factor = r;
						possibleUnits.add(unit);
						break;
					}
				}

				if (squarefulCheck && gaussianFactors.contains(factor)) {
					// System.out.println("Warning! Squareful number! " + this);
					return null;
				}
				gaussianFactors.add(factor);
			}
		}

		for (GaussianInteger unit : GAUSSIAN_UNITS) {
			if (product(gaussianFactors).multiply(
					product(possibleUnits).multiply(unit)).equals(this)) {
				possibleUnits.add(unit);
				break;
			}
		}

		// Add only one unit, not all of them
		gaussianFactors.add(product(possibleUnits));

		// Self checks, can be removed later
		GaussianInteger product = product(gaussianFactors);
		if (product.equals(this)) {
			// System.out.println(gaussianFactors + " = " + product);
		} else {
			System.err.println(gaussianFactors + " = " + product + " != "
					+ this);
		}

		return gaussianFactors;
	}

	/**
	 * Get prime factors of a gaussian integer
	 * 
	 * @param initialNumber
	 * @return
	 */
	public ArrayList<GaussianInteger> getGaussianPrimeFactorization() {
		GaussianInteger initialNumber = this;
		if (initialNumber.isRealNumber()) {
			return getGaussianPrimeFactorizationOfRealNumber(initialNumber
					.getRealInteger());
		}

		if (initialNumber.isGaussianUnit()) {
			return new ArrayList<>(
					Arrays.asList(new GaussianInteger[] { initialNumber }));
		}

		BigInteger initialNumberGreatestCommonDivisor = initialNumber
				.greatestCommonDivisorOfGaussianAandB();

		GaussianInteger reducedGaussianNumber = initialNumber
				.reduce(initialNumberGreatestCommonDivisor);

		BigInteger normalizedIntegerOfInitialNumber = multiplyByConjugateForNormalizedInteger(reducedGaussianNumber);

		ArrayList<GaussianInteger> reducedGaussianPrimeFactorization = getGaussianPrimeFactorizationOfRealNumber(normalizedIntegerOfInitialNumber);
		// I don't know why, but I have to remove the first unit
		reducedGaussianPrimeFactorization.remove(0);
		ArrayList<GaussianInteger> greatestCommonDivisorGaussianPrimeFactorization = getGaussianPrimeFactorizationOfRealNumber(initialNumberGreatestCommonDivisor);
		if (greatestCommonDivisorGaussianPrimeFactorization.size() == 0) {
			if (!initialNumberGreatestCommonDivisor.equals(IntegerUtils.ONE)) {
				reducedGaussianPrimeFactorization.add(new GaussianInteger(
						initialNumberGreatestCommonDivisor));
			}
		} else {
			reducedGaussianPrimeFactorization
					.addAll(greatestCommonDivisorGaussianPrimeFactorization);
		}

		ArrayList<GaussianInteger> finalCorrectGaussianPrimeFactors = new ArrayList<>();
		for (GaussianInteger bigComplex : reducedGaussianPrimeFactorization) {
			if (initialNumber.canEvenlyDivideBy(bigComplex) != null) {
				finalCorrectGaussianPrimeFactors.add(bigComplex);
			}
		}

		/*
		 * The following code adds units to the prime factorization. Not sure if
		 * needed yet.
		 */

		GaussianInteger product = GaussianInteger
				.product(finalCorrectGaussianPrimeFactors);
		if (!product.equals(initialNumber)) {

			for (GaussianInteger unit : GaussianInteger.GAUSSIAN_UNITS) {
				if (product.multiply(unit).equals(initialNumber)) {
					finalCorrectGaussianPrimeFactors.add(unit);
				}
			}
		}

		return finalCorrectGaussianPrimeFactors;
	}

	public BigInteger getImaginaryInteger() {
		return imaginaryInteger;
	}

	public BigInteger getRealInteger() {
		return realInteger;
	}

	/**
	 * For gaussian number G = a + bi, get GCD(a, b).
	 * 
	 * @param complexNumber
	 * @return
	 */
	public BigInteger greatestCommonDivisorOfGaussianAandB() {
		return IntegerUtils.greatestCommonDivisor(this.getRealInteger(),
				this.getImaginaryInteger());
	}

	@Override
	public int hashCode() {
		return this.getRealInteger().hashCode()
				+ this.getImaginaryInteger().hashCode();
	}

	public boolean isGaussianUnit() {
		return (realInteger.abs().equals(BigInteger.ONE) && imaginaryInteger
				.equals(BigInteger.ZERO))
				|| (imaginaryInteger.abs().equals(BigInteger.ONE) && realInteger
						.equals(BigInteger.ZERO));
	}

	/**
	 * Gaussian primes are Gaussian integers z=a+bi satisfying one of the
	 * following properties.
	 * 
	 * 1. If both a and b are nonzero then, a+bi is a Gaussian prime iff a^2+b^2
	 * is an ordinary prime.
	 * 
	 * 2. If a=0, then bi is a Gaussian prime iff |b| is an ordinary prime and
	 * |b|=3 (mod 4).
	 * 
	 * 3. If b=0, then a is a Gaussian prime iff |a| is an ordinary prime and
	 * |a|=3 (mod 4).
	 * 
	 * @return
	 */
	public boolean isPrime() {
		if (this.getImaginaryInteger().equals(BigInteger.ZERO)
				&& this.getRealInteger().equals(BigInteger.ZERO)) {
			return false; // Zero is not prime
		} else if (!this.getImaginaryInteger().equals(BigInteger.ZERO)
				&& !this.getRealInteger().equals(BigInteger.ZERO)) {
			if (IntegerUtils.isPrime(this.norm()) == null) {
				return true;
			}
		} else if (this.getRealInteger().equals(BigInteger.ZERO)) {
			BigInteger absB = this.getImaginaryInteger().abs();
			if (absB.mod(IntegerUtils.FOUR).equals(IntegerUtils.THREE)) {
				if (IntegerUtils.isPrime(absB) == null) {
					return true;
				}
			}
		} else if (this.getImaginaryInteger().equals(BigInteger.ZERO)) {
			BigInteger absA = this.getRealInteger().abs();
			if (absA.mod(IntegerUtils.FOUR).equals(IntegerUtils.THREE)) {
				if (IntegerUtils.isPrime(absA) == null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * A proper gaussian integer is z = a + bi and where a > 0 and b >= 0
	 * 
	 * @return
	 */
	public boolean isProperGuassian() {
		return realInteger.compareTo(BigInteger.ZERO) > 0
				&& imaginaryInteger.compareTo(BigInteger.ZERO) >= 0;
	}

	public boolean isRealNumber() {
		return imaginaryInteger.equals(BigInteger.ZERO);
	}

	public boolean isSquareFree() {
		if (isZero()) {
			return false;
		}
		if (isGaussianUnit()) {
			return true;
		}
		if (isPrime()) {
			return true;
		}
		ArrayList<GaussianInteger> fullFactorization = this
				.getGaussianPrimeFactorization();
		makeProperGaussianFactorization(fullFactorization);
		HashSet<GaussianInteger> uniqueFactorization = new HashSet<>(
				fullFactorization);
		System.out.println(fullFactorization + " - " + uniqueFactorization);
		return fullFactorization.size() == uniqueFactorization.size();
	}

	public boolean isZero() {
		return this.getRealInteger().equals(BigInteger.ZERO)
				&& this.getImaginaryInteger().equals(BigInteger.ZERO);
	}

	public ArrayList<GaussianInteger> getProperGaussianFactorization() {
		return makeProperGaussianFactorization(this
				.getGaussianPrimeFactorization());
	}

	private ArrayList<GaussianInteger> makeProperGaussianFactorization(
			ArrayList<GaussianInteger> factorization) {
		ArrayList<GaussianInteger> units = new ArrayList<GaussianInteger>();
		for (int i = 0; i < factorization.size(); i++) {
			GaussianInteger factor = factorization.get(i);
			if (factor.isGaussianUnit()) {
				factorization.remove(factor);
				units.add(factor);
				i--;
				continue;
			}
			if (factor.isProperGuassian()) {
				continue;
			}
			for (GaussianInteger unit : GAUSSIAN_UNITS) {
				GaussianInteger quotientOfUnit = factor.canEvenlyDivideBy(unit);
				if (quotientOfUnit.isProperGuassian()) {
					units.add(unit);
					factorization.set(i, quotientOfUnit);
					break;
				}
			}
		}
		factorization.add(product(units));
		return factorization;
	}

	public GaussianInteger multiply(final GaussianInteger b) {
		final GaussianInteger a = this;
		BigInteger real = a
				.getRealInteger()
				.multiply(b.getRealInteger())
				.subtract(
						a.getImaginaryInteger().multiply(
								b.getImaginaryInteger()));
		BigInteger imag = a.getRealInteger().multiply(b.getImaginaryInteger())
				.add(a.getImaginaryInteger().multiply(b.getRealInteger()));
		return new GaussianInteger(real, imag);
	}

	public GaussianInteger multiply(long a, long b) {
		return this.multiply(new GaussianInteger(a, b));
	}

	/**
	 * norm(z) where z = a + bi
	 * 
	 * @return a^2 + b^2
	 */
	public BigInteger norm() {
		return this.getRealInteger().pow(2)
				.add(this.getImaginaryInteger().pow(2));
	}

	/**
	 * Reduce a gaussian number by a common divisor
	 * 
	 * @param commonDivisor
	 * @return
	 */
	public GaussianInteger reduce(BigInteger commonDivisor) {
		GaussianInteger initialNumber = this;
		return new GaussianInteger(initialNumber.getRealInteger().divide(
				commonDivisor), initialNumber.getImaginaryInteger().divide(
				commonDivisor));
	}

	// return a string representation of the invoking Complex object
	public String toString() {
		if (imaginaryInteger.equals(BigInteger.ZERO))
			return realInteger.toString();
		if (realInteger.equals(BigInteger.ZERO)) {
			if (imaginaryInteger.equals(BigInteger.ONE)) {
				return "i";
			}
			return imaginaryInteger + "i";
		}

		BigInteger abs = imaginaryInteger.abs();
		String i = (abs.equals(BigInteger.ONE)) ? "i" : imaginaryInteger.abs()
				+ "i";

		if (imaginaryInteger.compareTo(BigInteger.ZERO) < 0)
			return realInteger + " - " + i;
		return realInteger + " + " + i;
	}
}

class IntegerUtils {

	static final BigInteger FOUR = BigInteger.valueOf(4);
	private static final BigInteger IS_PRIME = null;
	static final BigInteger ONE = BigInteger.ONE;
	static final BigInteger SIX = BigInteger.valueOf(6);
	static final BigInteger THREE = BigInteger.valueOf(3);
	static final BigInteger TWO = BigInteger.valueOf(2);
	static final BigInteger ZERO = BigInteger.ZERO;

	public static BigInteger bigIntSqRootFloor(BigInteger x)
			throws IllegalArgumentException {
		if (x.compareTo(ZERO) < 0) {
			throw new IllegalArgumentException("Negative argument.");
		}
		// square roots of 0 and 1 are trivial and
		// y == 0 will cause a divide-by-zero exception
		if (x.equals(ZERO) || x.equals(ONE)) {
			return x;
		} // end if
		BigInteger y;
		// starting with y = x / 2 avoids magnitude issues with x squared
		for (y = x.divide(TWO); y.compareTo(x.divide(y)) > 0; y = ((x.divide(y))
				.add(y)).divide(TWO))
			;
		return y;
	}

	public static BigInteger divideRound(BigDecimal dividend, BigDecimal divisor) {
		return dividend.divide(divisor, 0, RoundingMode.HALF_EVEN)
				.toBigInteger();
	}

	public static BigInteger divideRound(BigInteger dividend, BigInteger divisor) {
		return divideRound(new BigDecimal(dividend), new BigDecimal(divisor));
	}

	public static BigInteger greatestCommonDivisor(BigInteger x, BigInteger y) {

		BigInteger answer = IntegerUtils.ONE;

		if (x.compareTo(y) > 0) {
			x = x.add(y);
			y = x.subtract(y);
			x = x.subtract(y);
		}

		if (!x.equals(IntegerUtils.ZERO)
				&& y.equals(IntegerUtils.divideRound(y, x).multiply(x))) {
			answer = x;
		} else {

			for (BigInteger i = IntegerUtils.divideRound(x, IntegerUtils.TWO); i
					.compareTo(IntegerUtils.ONE) > 0; i = i
					.subtract(IntegerUtils.ONE)) {

				if (x.equals(IntegerUtils.divideRound(x, i).multiply(i)))

					if (y.equals(IntegerUtils.divideRound(y, i).multiply(i))) {
						answer = i;
						break;
					}
			}
		}

		return answer;
	}

	public static BigInteger greatestCommonDivisor(long x, long y) {
		return greatestCommonDivisor(BigInteger.valueOf(x),
				BigInteger.valueOf(y));
	}

	public static BigInteger isPrime(BigInteger n) {
		n = n.abs();
		if (n.compareTo(TWO) < 0)
			return ONE;
		if (n.equals(TWO))
			return IS_PRIME;
		if (n.equals(THREE))
			return IS_PRIME;
		if (n.mod(TWO).equals(ZERO))
			return TWO;
		else if (n.mod(THREE).equals(ZERO))
			return THREE;
		BigInteger sqrtN = bigIntSqRootFloor(n).add(ONE);

		for (BigInteger i = SIX; i.compareTo(sqrtN) <= 0; i = i.add(SIX)) {
			if (n.mod(i.subtract(ONE)).equals(ZERO)) {
				return i.subtract(ONE);
			} else if (n.mod(i.add(ONE)).equals(ZERO)) {
				return i.add(ONE);
			}
		}
		return IS_PRIME;
	}

	private static boolean isUnit(BigInteger value) {
		return value.abs().equals(ONE);
	}

	public static ArrayList<BigInteger> primeFactors(BigInteger value) {
		return primeFactors(value, false);
	}

	private static ArrayList<BigInteger> primeFactors(BigInteger value,
			boolean unique) {

		// If is prime, return blank list
		if (isPrime(value) == IS_PRIME || isUnit(value)) {
			ArrayList<BigInteger> primeReturn = new ArrayList<BigInteger>();
			primeReturn.add(value);
			return primeReturn;
		}

		Collection<BigInteger> tempPrimeFactors = new HashSet<>();
		if (!unique) {
			tempPrimeFactors = new ArrayList<>();
		}

		if (value.compareTo(ZERO) < 0) {
			tempPrimeFactors.add(ONE.negate());
			value = value.abs();
		}

		while (value.mod(TWO).equals(ZERO)) {
			tempPrimeFactors.add(TWO);
			value = value.divide(TWO);
		}

		BigInteger i = ONE;
		while (value.compareTo(ONE) > 0) {
			i = i.add(TWO);
			while (value.mod(i).equals(ZERO)) {
				if (isPrime(i) == IS_PRIME) {
					tempPrimeFactors.add(i);
				}
				value = value.divide(i);
			}
			BigInteger checkPrime = isPrime(value);
			if (checkPrime == IS_PRIME) {
				tempPrimeFactors.add(value);
				break;
			} else {
				i = checkPrime.subtract(TWO);
			}

		}

		ArrayList<BigInteger> primeFactors = new ArrayList<BigInteger>(
				tempPrimeFactors);
		primeFactors.sort(new Comparator<BigInteger>() {
			@Override
			public int compare(BigInteger o1, BigInteger o2) {
				return o1.compareTo(o2);
			}
		});
		return primeFactors;
	}

	public static ArrayList<BigInteger> uniquePrimeFactors(BigInteger value) {
		return primeFactors(value, true);
	}

}