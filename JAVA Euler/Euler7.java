
public class Euler7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int currentPrimeIndex = 2;
		long currentPrime = 3;
		long currentTest = currentPrime ;
		while(currentPrimeIndex < 10001){
			currentTest += 2;
			if(isPrime(currentTest)){
				currentPrime = currentTest;
				currentPrimeIndex++;
			}
		}
		
		System.out.println(currentPrime + " (" + currentPrimeIndex + ")" );

	}
	
	public static boolean isPrime(long n) {
		if (n < 2)
			return false;
		if (n == 2 || n == 3)
			return true;
		if (n % 2 == 0 || n % 3 == 0)
			return false;
		long sqrtN = (long) Math.sqrt(n) + 1;
		for (long i = 6L; i <= sqrtN; i += 6) {
			if (n % (i - 1) == 0 || n % (i + 1) == 0)
				return false;
		}
		return true;
	}

}
