package euler

import ch.qos.logback.classic.Level
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory


object Euler14 extends App {
  private val loggerInstance = LoggerFactory.getLogger(Euler14.getClass.getSimpleName)
    .asInstanceOf[ch.qos.logback.classic.Logger]

  loggerInstance.setLevel(Level.DEBUG)
  val logger = Logger(loggerInstance)

  logger.info("Starting Collatz Sequence generator...")

  var longestLength = -1
  var longestIndex = BigInt(-1)
  var longestSequence: Array[BigInt] = _
  val max = 1000000
  for (i <- 100000 to max) {
    if (loggerInstance.isDebugEnabled && i % 1000 == 0) {
      logger.debug("Sequencing {} {}%...", i, i.asInstanceOf[Double] * 100 / max)
    }
    val sequence = collatzSequence(i)
    val length = sequence.length
    if (length > longestLength){
      longestLength = length
      longestIndex = i
      longestSequence = sequence
    }
  }

  logger.info("Longest sequence is for {} with length {}.", longestIndex, longestLength)
  logger.info("Longest sequence is {}", longestSequence)

  def collatzSequence(num: Int): Array[BigInt] = {
    var arr: Array[BigInt] = Array()
    var n = BigInt(num)
    while (n > 0) {
      arr :+= n
      if (n == 1) {
        n = 0
      } else if (isEven(n)) {
        n = n / 2
      } else {
        n = (3 * n) + 1
      }
    }
    logger.trace("Sequence is {}.", arr)
    arr
  }

  def isEven(num: BigInt): Boolean = {
    num % 2 == 0
  }
}
